/* Author: 
    Ninad Parkar
*/

var 
formSubmit = document.getElementById('formSubmit'), //Submit Button
modal = document.getElementById('myModal'), //modal
modalCaption = document.getElementById('caption'), //For changing the message
close = document.querySelector('.close'), //For closing the modal
hamburger = document.querySelector('.hamburger'), //For Hamburger Icon in mobile view
nav = document.querySelector('nav'); //For nav

//For Form Validation
formSubmit.onclick = (e) => {
    var
    firstName = document.getElementById('first_name').value, //Firstname
    lastName = document.getElementById('last_name').value, //Lastname
    position = document.getElementById('position').value, //Position
    company = document.getElementById('company').value, //Company
    companyType = document.getElementById('company_type').value, //Company Type
    country = document.getElementById('country').value, //Country
    workMail = document.getElementById('work_mail').value, //Work Mail
    textValidation = /^[A-Za-z]+$/, //Regx for firstname and lastname
    textValidation2 = /^[A-Za-z]+[A-Za-z\s]+$/, //Regx for position and company
    emailValidaion = /^[a-zA-Z0-9+_.-]+@[a-zA-Z]+\.[a-zA-Z]+$/; //Regx for email

    e.preventDefault(); //Preventing deafult functionality of form

    if (!textValidation.test(firstName)) {
        modal.style.display = 'block';
        modalCaption.innerHTML = 'Invalid First Name field';
    }
    else if (!textValidation.test(lastName)) {
        modal.style.display = 'block';
        modalCaption.innerHTML = 'Invalid Last Name field';
    }
    else if (!textValidation2.test(position)) {
        modal.style.display = 'block';
        modalCaption.innerHTML = 'Invalid Position field';
    }
    else if (!textValidation2.test(company)) {
        modal.style.display = 'block';
        modalCaption.innerHTML = 'Invalid Company field';
    }
    else if (companyType == 'select') {
        modal.style.display = 'block';
        modalCaption.innerHTML = 'Select the Company Type';
    }
    else if (country == 'select') {
        modal.style.display = 'block';
        modalCaption.innerHTML = 'Select the Country';
    }
    else if (!emailValidaion.test(workMail)) {
        modal.style.display = 'block';
        modalCaption.innerHTML = 'Invalid Email field';
    }
    //If all the data is valid
    else {
        modal.style.display = 'block';
        modalCaption.innerHTML = 'Success';
    }

}

//For closing the modal
close.onclick = () => modal.style.display = 'none';

//For hamburger icon to toggle menu in mobile view
hamburger.onclick = function() {
    if(nav.style.display == 'block') {
        nav.style.display = 'none';
    }
      else {
        nav.style.display = 'block';
    }
}










































